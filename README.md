# 20232024

[[_TOC_]]

## LAB Access
### JUMP server
You can reach `phobos` jump server either via SSH or RDP protocol:

**SSH access**
```
# PHOBOS
ssh 10.233.130.190 -l YOUR_USERNAME -p 20142
```

**RDP access**
```
10.233.130.190:20141
```

NOTE: for authentication use your DOMAIN credentials.

<br></br>

### vCenters
There are 2 vCenter within DualEdu LAB:

**1. OLD vCenter**
```
172.27.16.4 vcenter01.dual.edu
```

**2. Adjust /etc/hosts NEW vCenter**
```
10.233.130.190 connect.dual.edu
```

**3. New vCenter from MAC**
```
https://vc.dual.sk-kosice.t-systems.sk/
```

NOTE: Login via dualedu domain credentials (wiw/passowrd).

**4. Connect to Guacamole**
```
https://connect.dual.edu/

pbarczi/D*1***
```

<br>

## Zadania
- [Zadanie 1](https://gitlab.com/dualedu/sisl/20232024/-/blob/main/zadania/Zadanie1.md)
- [Zadanie 2](https://gitlab.com/dualedu/sisl/20232024/-/blob/main/zadania/Zadanie2.md)

<br>

## Tutorials
- [Docker](https://gitlab.com/dualedu/sisl/20232024/-/blob/main/docker.md)
