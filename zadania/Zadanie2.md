# zadanie-2
Create own Docker image, push it into public docker repository and start container from it on dedicated deployment server.
You can check https://gitlab.com/dualedu/sisl/20232024#tutorials for help.


##### 1. Note down your port
Just take one of listed ports, it will be needed later for starting container.
```
server2a1  8001
server2a2  8002
server2a3  8003 
server2a4  8004 
server2a5  8005 
server2a6  8006 
server2a7  8007 
server2a8  8008 
server2a9  8009
server2a10 8010
server2a11 8011
server2a12 8012
server2a13 8013 
server2a14 8014 
server2a15 8015 pbarczi
```
<br>

##### 2. Build your own Docker image
Use your developer's server in order to build Image (server2aX)

As a source code you the one found on github:
https://github.com/ritaly/HTML-CSS-CV-demo

NOTE: your Image must contain your real info. Bonus point for also changing images and formal content of your web page.

<br>

##### 3. Push your Docker image into Docker registy
Use the following scheme for NAME while building Image
```
user_name/mycv:1.0

e.g.
petyb/mycv:1.0
```

<br>

##### 4. Start Docker container from your Docker image
**4.1.** Connect to deployment server
```
ssh cloud_user@18.170.78.17

Password: Start123
```
**NOTE:**
It is public machine accessible via Internet, in case of issues to reach it you can access it via `phobos`.

**4.2.** Start docker container from your image

Use your dedicated port while starting container!!!

Properly name your container e.g.: 
```
--name pbarczi-cv
```
