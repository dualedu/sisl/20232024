# zadanie-1


##### 1. Connect to your VM
```
ssh student@server2aX

Password: Dua1edu

You can switch to root acount using the same password.
```
> NOTE: 
- VMs are reachable from `phobos`
- choose your VM
- replace `X` with your actual VM number e.g `server2a10` according to the following list:

```
server2a1  0
server2a2  1
server2a3  1 
server2a4  1 
server2a5  1 
server2a6  1 
server2a7  1 
server2a8  x 
server2a9  0
server2a10 1
server2a11 1
server2a12 1
server2a13 x 
server2a14 x 
```

##### 2. Create user `tux`
Create local user as follows:
```
username: tux
uid:      666
comment:  TUX user
shellL    /bin/bash
password: Dua1edu
```

##### 3. Account expiration
Set Account expiration for user `tux` on: `31.12.2025`

##### 4. sudo rights
Grant user `tux` root's rights via sudo.

##### 5. Deploy SSH-key for user `tux`
`ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5`

##### 6. Identify attached extra HDDs and create filesystem as requested
```
> HDD1
- take whole disk 
- filesystem:               xfs
- size:                     1GB
- mountopoint:              /music

> HDD2 and HDD3
- join them into LVM
- create VolumeGroup        data

  > LogicalVolume 1.        photos
    filesystem              ext4
    size                    500MB
    mountopoint             /photos

  > LogicalVolum 2.         school
    filesystem              ext4
    size                    1GB
    mountpoint              /school
```
> NOTE: all filesystems must boot automatically!


##### 6. Install Packages
Here is the list of Packages which need to be installed:

```
telnet
git
vim
joe
curl
screen 
```

##### 7. Install and Configure Web server
- Ensure that `nginx` package is installed
- Ensure that service `nginx` is running
- Ensure that service `nginx` is enabled at boot time


##### 8. Create web page
Create simple web page served by your nginx with the following content:
```
<!DOCTYPE html>
<html>
<body>

<h1>Hello from Webserver server2aX</h1>
<p>your@companyemail</p>

</body>
</html>
```
NOTE: Adjust your machine name accordingly.


##### 9. Test your web page
```
http://server2aX
```
> NOTE: Your web page should be accessible from `phobos` via any kind of web browser.


##### 10. Final reboot
```
reboot
```

##### 11. Perfom Ubuntu OS update
Ensure that all available updates are installed so your Ubuntu OS is up-to-date.
