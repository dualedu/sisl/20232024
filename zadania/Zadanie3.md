# zadanie-3
Perform disk management Tasks following the given instructions:

<br>

##### 1. Access target servers
Tasks need to be performed on the following machines:
```
server1.dualedu.sk
server2.dualedu.sk
```

_NOTE:_
- you can access those machines via SSH as user "tux" with password "Dua1edu".
- this user has also root permissions, so you can switch to user root if needed.

<br>

## PART I.

On **server1.dualedu.sk** perform the following **Tasks**:
- there are several HDDs attached to this server, pick one HDD on which you will perform Tasks:

1. split your HDD into 2 physical partitions with cca equal size each
2. create filesystems as follows:
```
# Partition 1
filesystem: ext3
mountpoint: /<YOUR-WIW>/data1

# Partition 2
filesystem: ext4
mountpoint: /<YOUR-WIW>/data2
```

3. set permisisons 775 on above created mountpoints

4. within new filesystems create file named `testfile` which will contain your email address (for both `data1` and `data2`)

<br>

### PART II.

On **server2.dualedu.sk** perform the following **Tasks**:

- there are several HDDs attached to this server, pick one HDD on which you will perform Tasks:

1. from your HDD create LVM setup as follows
```
VG name: <YOUR-WIW>

# LV 1
LV name: data1
filesystem: ext4
mountpoint /<YOUR-WIW>/data1

# LV 2
LV name: data2
filesystem: xfs
mountpoint /<YOUR-WIW>/data2
```

2. set permisisons 775 on above created mountpoints

3. within new filesystems create file named testfile which will contain your email address (for both `data1` and `data2`)
